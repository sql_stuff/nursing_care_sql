CREATE TABLE Place(
    placeID serial PRIMARY KEY NOT NULL,
    street varchar(255) NOT NULL,
    house_number integer NOT NULL,
    plz varchar(255) NOT NULL,
    place_name VARCHAR(255) NOT NUll
);
CREATE TABLE nurse(
    nurse_ID serial PRIMARY KEY,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    street varchar(255) NOT NULL,
    house_number integer NOT NULL,
    plz varchar(255) NOT NULL,
    telefon_number varchar(255) NOT NULL
);
CREATE TABLE Nursing_care_services(
    LeistungsID serial PRIMARY KEY,
    description varchar(255) NOT NULL,
    costs NUMERIC(8,2) NOT NULL,
    planned_time_expenditure integer NOT NULL
);
CREATE TABLE Company_car(
    CarID serial PRIMARY KEY,
    kilometres_driven integer not NULL,
    assignment_date date NOT NULL,
    licence_plate varchar(255) NOT NULL
);

CREATE TABLE Distance(
    distanceID serial PRIMARY KEY NOT NULL,
    start_point serial REFERENCES Place(placeID) NOT NULL,
    end_point serial REFERENCES Place(placeID) NOT NULL,
    distanceInKilometers integer NOT NULL 
);

CREATE TABLE Patient(
    PatientNumber serial PRIMARY KEY,
    date_of_birth date NOT NULL,
    residence serial REFERENCES Place(placeID) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    telefon_number VARCHAR(255) NOT NULL  
);

CREATE TABLE Angehöriger (
  AgID serial PRIMARY KEY,
  associated_patient serial REFERENCES Patient(PatientNumber) not NULL,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  telefon_number varchar(255) NOT NULL
);

CREATE TABLE care_assignemt(
    BillID serial PRIMARY KEY,
    Pflegeleistung serial REFERENCES nursing_care_services(LeistungsID) NOT NULL,
    cared_for_patient serial REFERENCES Patient(PatientNumber) NOT NULL,
    nurse serial REFERENCES nurse(nurse_ID) NOT NULL,
    journey serial REFERENCES Distance(distanceID) NOT NULL,
    used_Company_car serial REFERENCES Company_car(CarID) NOT NULL,
    driven_date date NOT NULL,
    driven_time integer NOT NULL
);

CREATE TABLE Valuations(
    val_ID serial PRIMARY KEY,
    Pflegeleistung serial REFERENCES nursing_care_services(LeistungsID) NOT NULL,
    cared_for_patient serial REFERENCES Patient(PatientNumber) NOT NULL,
    used_Company_car serial REFERENCES Company_car(CarID) NOT NULL,
    nurse serial REFERENCES nurse(nurse_ID) NOT NULL,
    val_date date NOT NULL,
    speed integer NOT NULL,
    Kindness integer NOT NULL,
    Competence integer NOT NULL,
    Reliability integer NOT NULL,
    communication integer NOT NULL
);

CREATE TABLE Health_insurance_company(
    company_ID serial Primary KEY NOT NULL,
    associated_patient serial REFERENCES Patient(PatientNumber) NOT NULL, 
    company_name VARCHAR(255) NOT NULL
);

CREATE TABLE billings(
    bill_ID serial PRIMARY KEY,
    kassenID serial REFERENCES Health_insurance_company(company_ID) NOT NULL,
    assignemtID serial REFERENCES care_assignemt(BillID) NOT NULL 
);
