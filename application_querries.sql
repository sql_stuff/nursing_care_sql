SELECT nurse.first_name, nurse.last_name, AVG(care_assignemt.journey) AS average_driving_distance_per_assignment
FROM Nurse nurse
JOIN care_assignemt ON care_assignemt.nurse = nurse.nurse_ID
GROUP BY nurse.nurse_ID
ORDER BY average_driving_distance_per_assignment DESC
LIMIT 1;

SELECT patient.first_name, patient.last_name, SUM(nursing_care_services.costs) AS total_care_costs
FROM Patient patient
JOIN care_assignemt ON care_assignemt.cared_for_patient = patient.PatientNumber
JOIN Nursing_care_services ON care_assignemt.Pflegeleistung = Nursing_care_services.LeistungsID
GROUP BY patient.PatientNumber;

SELECT nursing_care_services.description, AVG(care_assignemt.driven_time) AS average_time_per_service
FROM Nursing_care_services nursing_care_services
JOIN care_assignemt ON care_assignemt.Pflegeleistung = nursing_care_services.LeistungsID
GROUP BY nursing_care_services.LeistungsID;

SELECT company_car.licence_plate, COUNT(*) AS frequency
FROM company_car
JOIN care_assignemt ON care_assignemt.used_Company_car = company_car.CarID
GROUP BY company_car.CarID
ORDER BY frequency DESC
LIMIT 1;

SELECT nurse.first_name, nurse.last_name, SUM(care_assignemt.journey) AS total_distance_travelled
FROM Nurse nurse
JOIN care_assignemt ON care_assignemt.nurse = nurse.nurse_ID
GROUP BY nurse.nurse_ID;

SELECT health_insurance_company.company_name, SUM(care_assignemt.driven_time) AS total_care_costs
FROM Health_insurance_company health_insurance_company
JOIN Patient patient ON patient.PatientNumber = health_insurance_company.associated_patient
JOIN care_assignemt ON care_assignemt.cared_for_patient = patient.PatientNumber
GROUP BY health_insurance_company.company_name;

SELECT nurse.first_name, nurse.last_name, COUNT(*) AS number_of_care_services
FROM Nurse nurse
JOIN care_assignemt ON care_assignemt.nurse = nurse.nurse_ID
GROUP BY nurse.nurse_ID, care_assignemt.cared_for_patient
ORDER BY COUNT(*) DESC
LIMIT 1;

SELECT licence_plate, AVG(kilometres_driven) AS average_mileage FROM Company_car
GROUP BY licence_plate;
