INSERT INTO Place(street, house_number, plz, place_name)
VALUES
    ('123 Main Street', 123, '12345', 'Patient Residence'),
    ('456 Elm Street', 456, '56789', 'Nursing Home'),
    ('udo Street', 23, '93560','Headquarter');
INSERT INTO Nurse(first_name, last_name, street, house_number, plz, telefon_number)
VALUES
    ('John', 'Doe', '333 Maple Street', 333, '98765', '0123456789'),
    ('Jane', 'Smith', '789 Oak Street', 789, '45678', '9876543210');
INSERT INTO Nursing_care_services(description, costs, planned_time_expenditure)
VALUES
    ('Personal hygiene', 15.00, 30),
    ('Meal preparation', 10.00, 20),
    ('Medication management', 12.00, 30);
INSERT INTO Company_car(kilometres_driven,licence_plate, assignment_date)
VALUES
    ('300','ABC-123', '2023-10-04'),
    ('300','DEF-456', '2023-08-28');
INSERT INTO Distance(start_point, end_point, distanceInKilometers)
VALUES
    (1, 2, 10),
    (1, 3, 10),
    (2, 1, 10),
    (2, 3, 20),
    (3, 2, 20),
    (3, 1, 10);
INSERT INTO Patient(date_of_birth,residence, first_name, last_name, telefon_number)
VALUES
    (TO_DATE('1994-10-04', 'YYYY-MM-DD'),1,'Mary', 'Jones', '0987654321'),
    (TO_DATE('1980-10-04', 'YYYY-MM-DD'),2,'Peter', 'Williams', '1234567890');
INSERT INTO Angehöriger(associated_patient, first_name, last_name, telefon_number)
VALUES
    (1, 'Susan', 'Johnson', '2345678901'),
    (2, 'David', 'Robinson', '3456789012');
INSERT INTO care_assignemt(Pflegeleistung, cared_for_patient, nurse, journey, used_Company_car, driven_date, driven_time)
VALUES
    (1, 1, 1, 1, 1, '2023-10-04', 2),
    (2, 2, 2, 2, 2, '2023-08-28', 3);
INSERT INTO Health_insurance_company(associated_patient,company_name)
VALUES
    (1,'Aetna'),
    (2,'BCBS');
INSERT INTO Valuations (val_date, speed, Kindness, Competence, Reliability, communication, Pflegeleistung, cared_for_patient, used_Company_car, nurse)
VALUES
  ('2023-10-05', 10, 10, 10, 10, 10, 2, 2, 2, 2),
  ('2023-10-07', 8, 8, 8, 8, 8, 3, 1, 1, 1),
  ('2023-10-10', 6, 6, 6, 6, 6, 1, 2, 2, 2),
  ('2023-10-12', 4, 4, 4, 4, 4, 2, 1, 1, 1),
  ('2023-10-15', 1, 1, 1, 1, 1, 3, 2, 2, 2);
