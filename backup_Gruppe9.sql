--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 16.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: anstellungsart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.anstellungsart (
    aart integer NOT NULL,
    bezeichnung character varying,
    wochenstunden integer
);


ALTER TABLE public.anstellungsart OWNER TO postgres;

--
-- Name: anstellungsart_aart_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.anstellungsart_aart_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.anstellungsart_aart_seq OWNER TO postgres;

--
-- Name: anstellungsart_aart_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.anstellungsart_aart_seq OWNED BY public.anstellungsart.aart;


--
-- Name: lieferant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lieferant (
    lnr integer NOT NULL,
    bnr integer,
    preis integer,
    lieferantname character varying
);


ALTER TABLE public.lieferant OWNER TO postgres;

--
-- Name: lieferant_lnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lieferant_lnr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.lieferant_lnr_seq OWNER TO postgres;

--
-- Name: lieferant_lnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lieferant_lnr_seq OWNED BY public.lieferant.lnr;


--
-- Name: materialbedarf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materialbedarf (
    bnr integer NOT NULL,
    tnr integer,
    material character varying,
    menge integer
);


ALTER TABLE public.materialbedarf OWNER TO postgres;

--
-- Name: materialbedarf_bnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.materialbedarf_bnr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.materialbedarf_bnr_seq OWNER TO postgres;

--
-- Name: materialbedarf_bnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.materialbedarf_bnr_seq OWNED BY public.materialbedarf.bnr;


--
-- Name: mitarbeiter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mitarbeiter (
    mnr integer NOT NULL,
    aart integer,
    vorname character varying,
    nachname character varying,
    "position" character varying
);


ALTER TABLE public.mitarbeiter OWNER TO postgres;

--
-- Name: mitarbeiter_mnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mitarbeiter_mnr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.mitarbeiter_mnr_seq OWNER TO postgres;

--
-- Name: mitarbeiter_mnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mitarbeiter_mnr_seq OWNED BY public.mitarbeiter.mnr;


--
-- Name: teilprojekt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teilprojekt (
    tnr integer NOT NULL,
    bezeichnung character varying,
    "arbeitsstundenschätzung" integer,
    budget integer
);


ALTER TABLE public.teilprojekt OWNER TO postgres;

--
-- Name: teilprojekt_tnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teilprojekt_tnr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.teilprojekt_tnr_seq OWNER TO postgres;

--
-- Name: teilprojekt_tnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teilprojekt_tnr_seq OWNED BY public.teilprojekt.tnr;


--
-- Name: teilprojektfortschritt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teilprojektfortschritt (
    fnr integer NOT NULL,
    tnr integer,
    mnr integer,
    datum date,
    arbeitszeit integer
);


ALTER TABLE public.teilprojektfortschritt OWNER TO postgres;

--
-- Name: teilprojektfortschritt_fnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teilprojektfortschritt_fnr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.teilprojektfortschritt_fnr_seq OWNER TO postgres;

--
-- Name: teilprojektfortschritt_fnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teilprojektfortschritt_fnr_seq OWNED BY public.teilprojektfortschritt.fnr;


--
-- Name: teilprojektzuweisung; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teilprojektzuweisung (
    znr integer NOT NULL,
    tnr integer,
    mnr integer
);


ALTER TABLE public.teilprojektzuweisung OWNER TO postgres;

--
-- Name: teilprojektzuweisung_znr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teilprojektzuweisung_znr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.teilprojektzuweisung_znr_seq OWNER TO postgres;

--
-- Name: teilprojektzuweisung_znr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teilprojektzuweisung_znr_seq OWNED BY public.teilprojektzuweisung.znr;


--
-- Name: unfall; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unfall (
    unr integer NOT NULL,
    tnr integer,
    mnr integer,
    folgen character varying,
    supervisormnr integer
);


ALTER TABLE public.unfall OWNER TO postgres;

--
-- Name: unfall_unr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unfall_unr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.unfall_unr_seq OWNER TO postgres;

--
-- Name: unfall_unr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unfall_unr_seq OWNED BY public.unfall.unr;


--
-- Name: anstellungsart aart; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anstellungsart ALTER COLUMN aart SET DEFAULT nextval('public.anstellungsart_aart_seq'::regclass);


--
-- Name: lieferant lnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lieferant ALTER COLUMN lnr SET DEFAULT nextval('public.lieferant_lnr_seq'::regclass);


--
-- Name: materialbedarf bnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materialbedarf ALTER COLUMN bnr SET DEFAULT nextval('public.materialbedarf_bnr_seq'::regclass);


--
-- Name: mitarbeiter mnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mitarbeiter ALTER COLUMN mnr SET DEFAULT nextval('public.mitarbeiter_mnr_seq'::regclass);


--
-- Name: teilprojekt tnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojekt ALTER COLUMN tnr SET DEFAULT nextval('public.teilprojekt_tnr_seq'::regclass);


--
-- Name: teilprojektfortschritt fnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektfortschritt ALTER COLUMN fnr SET DEFAULT nextval('public.teilprojektfortschritt_fnr_seq'::regclass);


--
-- Name: teilprojektzuweisung znr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektzuweisung ALTER COLUMN znr SET DEFAULT nextval('public.teilprojektzuweisung_znr_seq'::regclass);


--
-- Name: unfall unr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unfall ALTER COLUMN unr SET DEFAULT nextval('public.unfall_unr_seq'::regclass);


--
-- Data for Name: anstellungsart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.anstellungsart (aart, bezeichnung, wochenstunden) FROM stdin;
1	Vollzeit	40
2	Teilzeit	30
3	Werkstudent	20
\.


--
-- Data for Name: lieferant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lieferant (lnr, bnr, preis, lieferantname) FROM stdin;
1	1	500000	MercLogistics
2	2	750000	MercLogistics
3	3	600000	PowerTransport
4	4	300000	CosmoMetals
5	5	400000	MercLogistics
6	6	250000	MercLogistics
\.


--
-- Data for Name: materialbedarf; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materialbedarf (bnr, tnr, material, menge) FROM stdin;
1	1	Titanium Alloy	1000
2	2	High-Strength Steel	1500
3	3	Advanced Composite Panels	800
4	2	Aluminum Alloy	1000
5	2	Titanium Alloy	800
6	1	Titanium Alloy	500
\.


--
-- Data for Name: mitarbeiter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mitarbeiter (mnr, aart, vorname, nachname, "position") FROM stdin;
1	1	Josef	Müller	Supervisor
2	1	Marco	Pawlow	Arbeiter
3	2	Kira	Grein	Arbeiter
4	2	Norbert	Luft	Arbeiter
5	1	Mario	Winkel	Wachposten
6	1	Herbert	Bälz	Wachposten
7	3	Thorben	Holst	Arbeiter
8	3	Lutz	Eifler	Arbeiter
9	1	Jule	Tornow	Supervisor
10	2	Paniz	Osvaldo	Arbeiter
11	2	Jada	Neohne	Arbeiter
\.


--
-- Data for Name: teilprojekt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teilprojekt (tnr, bezeichnung, "arbeitsstundenschätzung", budget) FROM stdin;
1	Hauptstruktur	500	1000000
2	Lebenserhaltungssysteme	300	600000
3	Energieversorgung	400	800000
\.


--
-- Data for Name: teilprojektfortschritt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teilprojektfortschritt (fnr, tnr, mnr, datum, arbeitszeit) FROM stdin;
1	1	2	2023-12-19	8
2	2	3	2023-12-19	6
3	3	4	2023-12-19	6
4	2	7	2023-12-19	4
5	1	8	2023-12-19	4
6	2	10	2023-12-19	6
7	3	11	2023-12-19	6
8	1	2	2023-12-20	8
9	2	3	2023-12-20	6
10	3	4	2023-12-20	6
11	2	7	2023-12-20	4
12	1	8	2023-12-20	4
13	2	10	2023-12-20	6
14	3	11	2023-12-20	6
15	1	2	2023-12-21	8
16	2	3	2023-12-21	6
17	3	4	2023-12-21	6
18	2	7	2023-12-21	4
19	1	8	2023-12-21	4
20	2	10	2023-12-21	6
21	3	11	2023-12-21	6
22	1	2	2023-12-22	8
23	2	3	2023-12-22	6
24	3	4	2023-12-22	6
25	2	7	2023-12-22	4
26	1	8	2023-12-22	4
27	2	10	2023-12-22	6
28	3	11	2023-12-22	6
29	1	2	2024-01-02	8
30	2	3	2024-01-02	6
31	3	4	2024-01-02	6
32	2	7	2024-01-02	4
33	1	8	2024-01-02	4
34	2	10	2024-01-02	6
35	3	11	2024-01-02	6
36	1	2	2024-01-03	8
37	2	3	2024-01-03	6
38	3	4	2024-01-03	6
39	2	7	2024-01-03	4
40	1	8	2024-01-03	4
41	2	10	2024-01-03	6
42	3	11	2024-01-03	6
43	1	2	2024-01-04	8
44	2	3	2024-01-04	6
45	3	4	2024-01-04	6
46	2	7	2024-01-04	4
47	1	8	2024-01-04	4
48	2	10	2024-01-04	6
49	3	11	2024-01-04	6
50	1	2	2024-01-05	8
51	2	3	2024-01-05	6
52	3	4	2024-01-05	6
53	2	7	2024-01-05	4
54	1	8	2024-01-05	4
55	2	10	2024-01-05	6
56	3	11	2024-01-05	6
57	1	2	2024-01-08	8
58	2	3	2024-01-08	6
59	3	4	2024-01-08	6
60	2	7	2024-01-08	4
61	1	8	2024-01-08	4
62	2	10	2024-01-08	2
63	3	11	2024-01-08	6
64	1	2	2024-01-09	8
65	2	3	2024-01-09	6
66	3	4	2024-01-09	6
67	2	7	2024-01-09	4
68	1	8	2024-01-09	4
69	3	11	2024-01-09	6
70	1	2	2024-01-10	8
71	2	3	2024-01-10	6
72	3	4	2024-01-10	6
73	2	7	2024-01-10	4
74	1	8	2024-01-10	4
75	3	11	2024-01-10	6
76	1	2	2024-01-11	8
77	2	3	2024-01-11	6
78	3	4	2024-01-11	6
79	2	7	2024-01-11	4
80	1	8	2024-01-11	4
81	3	11	2024-01-11	6
82	1	2	2024-01-12	8
83	2	3	2024-01-12	6
84	3	4	2024-01-12	6
85	2	7	2024-01-12	4
86	1	8	2024-01-12	4
87	3	11	2024-01-12	6
88	1	2	2024-01-15	8
89	2	3	2024-01-15	6
90	3	4	2024-01-15	6
91	2	7	2024-01-15	4
92	1	8	2024-01-15	4
93	3	11	2024-01-15	6
94	1	2	2024-01-16	8
95	2	3	2024-01-16	6
96	3	4	2024-01-16	6
97	2	7	2024-01-16	4
98	1	8	2024-01-16	4
99	3	11	2024-01-16	6
100	1	2	2024-01-17	8
101	2	3	2024-01-17	6
102	3	4	2024-01-17	6
103	2	7	2024-01-17	4
104	1	8	2024-01-17	4
105	3	11	2024-01-17	6
106	1	2	2024-01-18	8
107	2	3	2024-01-18	6
108	3	4	2024-01-18	6
109	2	7	2024-01-18	4
110	1	8	2024-01-18	4
111	3	11	2024-01-18	6
112	1	2	2024-01-19	8
113	2	3	2024-01-19	6
114	3	4	2024-01-19	6
115	2	7	2024-01-19	4
116	1	8	2024-01-19	4
117	3	11	2024-01-19	6
118	1	2	2024-01-22	8
119	2	3	2024-01-22	6
120	3	4	2024-01-22	6
121	2	7	2024-01-22	4
122	1	8	2024-01-22	4
123	3	11	2024-01-22	6
124	1	2	2024-01-23	8
125	2	3	2024-01-23	6
126	3	4	2024-01-23	6
127	2	7	2024-01-23	4
128	1	8	2024-01-23	4
129	3	11	2024-01-23	6
130	1	2	2024-01-24	8
131	2	3	2024-01-24	6
132	3	4	2024-01-24	6
133	2	7	2024-01-24	4
134	1	8	2024-01-24	4
135	3	11	2024-01-24	6
136	1	2	2024-01-25	8
137	2	3	2024-01-25	6
138	3	4	2024-01-25	6
139	2	7	2024-01-25	4
140	1	8	2024-01-25	4
141	3	11	2024-01-25	6
142	1	2	2024-01-26	8
143	2	3	2024-01-26	6
144	3	4	2024-01-26	6
145	2	7	2024-01-26	4
146	1	8	2024-01-26	4
147	3	11	2024-01-26	6
148	1	2	2024-01-29	8
149	2	3	2024-01-29	6
150	3	4	2024-01-29	6
151	2	7	2024-01-29	4
152	1	8	2024-01-29	4
153	3	11	2024-01-29	6
154	1	2	2024-01-30	8
155	2	3	2024-01-30	6
156	3	4	2024-01-30	6
157	2	7	2024-01-30	4
158	1	8	2024-01-30	4
159	3	11	2024-01-30	6
\.


--
-- Data for Name: teilprojektzuweisung; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teilprojektzuweisung (znr, tnr, mnr) FROM stdin;
1	1	2
2	2	3
3	3	4
4	2	7
5	1	8
6	2	9
7	2	10
8	3	11
\.


--
-- Data for Name: unfall; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unfall (unr, tnr, mnr, folgen, supervisormnr) FROM stdin;
1	1	2	Gebrochenes Bein	1
2	1	2	Leichte Verletzungen	1
3	1	8	Leichte Verletzungen	1
4	2	3	Gebrochene Hand	9
5	2	3	Gebrochener Fuß	9
\.


--
-- Name: anstellungsart_aart_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.anstellungsart_aart_seq', 3, true);


--
-- Name: lieferant_lnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lieferant_lnr_seq', 6, true);


--
-- Name: materialbedarf_bnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.materialbedarf_bnr_seq', 6, true);


--
-- Name: mitarbeiter_mnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mitarbeiter_mnr_seq', 11, true);


--
-- Name: teilprojekt_tnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.teilprojekt_tnr_seq', 3, true);


--
-- Name: teilprojektfortschritt_fnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.teilprojektfortschritt_fnr_seq', 159, true);


--
-- Name: teilprojektzuweisung_znr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.teilprojektzuweisung_znr_seq', 8, true);


--
-- Name: unfall_unr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unfall_unr_seq', 5, true);


--
-- Name: anstellungsart anstellungsart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anstellungsart
    ADD CONSTRAINT anstellungsart_pkey PRIMARY KEY (aart);


--
-- Name: lieferant lieferant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lieferant
    ADD CONSTRAINT lieferant_pkey PRIMARY KEY (lnr);


--
-- Name: materialbedarf materialbedarf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materialbedarf
    ADD CONSTRAINT materialbedarf_pkey PRIMARY KEY (bnr);


--
-- Name: mitarbeiter mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mitarbeiter
    ADD CONSTRAINT mitarbeiter_pkey PRIMARY KEY (mnr);


--
-- Name: teilprojekt teilprojekt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojekt
    ADD CONSTRAINT teilprojekt_pkey PRIMARY KEY (tnr);


--
-- Name: teilprojektfortschritt teilprojektfortschritt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektfortschritt
    ADD CONSTRAINT teilprojektfortschritt_pkey PRIMARY KEY (fnr);


--
-- Name: teilprojektzuweisung teilprojektzuweisung_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektzuweisung
    ADD CONSTRAINT teilprojektzuweisung_pkey PRIMARY KEY (znr);


--
-- Name: unfall unfall_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unfall
    ADD CONSTRAINT unfall_pkey PRIMARY KEY (unr);


--
-- Name: lieferant lieferant_bnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lieferant
    ADD CONSTRAINT lieferant_bnr_fkey FOREIGN KEY (bnr) REFERENCES public.materialbedarf(bnr);


--
-- Name: materialbedarf materialbedarf_tnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materialbedarf
    ADD CONSTRAINT materialbedarf_tnr_fkey FOREIGN KEY (tnr) REFERENCES public.teilprojekt(tnr);


--
-- Name: mitarbeiter mitarbeiter_aart_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mitarbeiter
    ADD CONSTRAINT mitarbeiter_aart_fkey FOREIGN KEY (aart) REFERENCES public.anstellungsart(aart);


--
-- Name: teilprojektfortschritt teilprojektfortschritt_mnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektfortschritt
    ADD CONSTRAINT teilprojektfortschritt_mnr_fkey FOREIGN KEY (mnr) REFERENCES public.mitarbeiter(mnr);


--
-- Name: teilprojektfortschritt teilprojektfortschritt_tnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektfortschritt
    ADD CONSTRAINT teilprojektfortschritt_tnr_fkey FOREIGN KEY (tnr) REFERENCES public.teilprojekt(tnr);


--
-- Name: teilprojektzuweisung teilprojektzuweisung_mnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektzuweisung
    ADD CONSTRAINT teilprojektzuweisung_mnr_fkey FOREIGN KEY (mnr) REFERENCES public.mitarbeiter(mnr);


--
-- Name: teilprojektzuweisung teilprojektzuweisung_tnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teilprojektzuweisung
    ADD CONSTRAINT teilprojektzuweisung_tnr_fkey FOREIGN KEY (tnr) REFERENCES public.teilprojekt(tnr);


--
-- Name: unfall unfall_mnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unfall
    ADD CONSTRAINT unfall_mnr_fkey FOREIGN KEY (mnr) REFERENCES public.mitarbeiter(mnr);


--
-- Name: unfall unfall_supervisormnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unfall
    ADD CONSTRAINT unfall_supervisormnr_fkey FOREIGN KEY (supervisormnr) REFERENCES public.mitarbeiter(mnr);


--
-- Name: unfall unfall_tnr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unfall
    ADD CONSTRAINT unfall_tnr_fkey FOREIGN KEY (tnr) REFERENCES public.teilprojekt(tnr);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: pg_database_owner
--

GRANT ALL ON SCHEMA public TO "ManByPaz";


--
-- Name: TABLE anstellungsart; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.anstellungsart TO "ManByPaz";


--
-- Name: SEQUENCE anstellungsart_aart_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.anstellungsart_aart_seq TO "ManByPaz";


--
-- Name: TABLE lieferant; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.lieferant TO "ManByPaz";


--
-- Name: SEQUENCE lieferant_lnr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.lieferant_lnr_seq TO "ManByPaz";


--
-- Name: TABLE materialbedarf; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.materialbedarf TO "ManByPaz";


--
-- Name: SEQUENCE materialbedarf_bnr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.materialbedarf_bnr_seq TO "ManByPaz";


--
-- Name: TABLE mitarbeiter; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.mitarbeiter TO "ManByPaz";


--
-- Name: SEQUENCE mitarbeiter_mnr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.mitarbeiter_mnr_seq TO "ManByPaz";


--
-- Name: TABLE teilprojekt; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.teilprojekt TO "ManByPaz";


--
-- Name: SEQUENCE teilprojekt_tnr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.teilprojekt_tnr_seq TO "ManByPaz";


--
-- Name: TABLE teilprojektfortschritt; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.teilprojektfortschritt TO "ManByPaz";


--
-- Name: SEQUENCE teilprojektfortschritt_fnr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.teilprojektfortschritt_fnr_seq TO "ManByPaz";


--
-- Name: TABLE teilprojektzuweisung; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.teilprojektzuweisung TO "ManByPaz";


--
-- Name: SEQUENCE teilprojektzuweisung_znr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.teilprojektzuweisung_znr_seq TO "ManByPaz";


--
-- Name: TABLE unfall; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.unfall TO "ManByPaz";


--
-- Name: SEQUENCE unfall_unr_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.unfall_unr_seq TO "ManByPaz";


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON SEQUENCES TO "ManByPaz";


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES TO "ManByPaz";


--
-- PostgreSQL database dump complete
--

